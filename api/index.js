var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var Agency = require('./app/models/agency');
var mandrill = require('node-mandrill')('1r-M_OlgJNzpGQl2dFrnUg');
var cmd = require('node-cmd');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

var port = process.env.PORT || 3003;

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/casper');
var jobsFound = 0;

cmd.run('cd .. && casperjs scraper.js');
setInterval(() => {
    cmd.run('cd .. && casperjs scraper.js');
    jobsFound = 0;
}, 3600000)

var router = express.Router();

router.use(function (req, res, next) {
    // do logging
    console.log('Server Running...');
    next();
});

router.route('/state')
    .post(function (req, res) {
        _notifier(req.body.state);
        res.json({
            message: 'Server updated'
        });
    });

router.route('/agencies')
    .post(function (req, res) {
        var agency = new Agency();
        agency.url = req.body.url;
        agency.id = req.body.id;
        agency.description = req.body.description;
        agency.creator = req.body.creator;
        agency.address = req.body.address;
        agency.phone = req.body.phone;
        agency.discipline = req.body.discipline;
        agency.start_date = req.body.start_date;
        agency.duration = req.body.duration;
        agency.location = req.body.location;
        agency.rate = req.body.rate;
        Agency.find({
            id: agency.id
        }, function (err, docs) {
            if (docs.length) {
                console.log('Found match... Not saving!')
                console.log(agency.id)
                res.json({
                    message: 'Found match... Not saving!'
                });

            } else {
                console.log('No match found... Saving!')
                agency.save(function (err) {
                    if (err)
                        console.log(err);
                    else {
                        res.json({
                            message: 'No match found... Saving!'
                        });
                    }
                });
                console.log('Emailing!');
                sendMail(req.body);
                jobsFound += 1;
            }
        });
    });

//http://localhost:8080/rate/46692/250

app.use('/api', router);
app.use('/', express.static('public'));
app.listen(port);
console.log('Magic happens on port ' + port);
//
function sendMail(mail) {
    html = mail.creator + '<br><br>' + mail.url + '<br><br>' + mail.description +
        '<br><br><br><br> Details : <br> Descipline: ' + mail.discipline +
        '<br> Start date: ' + mail.start_date +
        '<br> Duration: ' + mail.duration +
        '<br> Location: ' + mail.location +
        mail.address + mail.phone;
    let location = mail.location.split('<br>').join('')
    mandrill('/messages/send', {
        message: {
            to: [{
                email: 'nathan@codeaddicts.io',
                name: 'Nathan Roberts'
            }, {
                email: 'oussama@codeaddicts.io',
                name: 'Oussama Zorgui'
            }],
            from_email: 'alerts@codeaddicts.io',
            subject: "YunoJuno - " + mail.title + ' - ' + mail.rate + ' - ' + location.replace('<div class="Rule"></div>', ''),
            html: html
        }
    }, function (error, response) {
        if (error) console.log(JSON.stringify(error));
        else console.log(response);
    });
}

function _notifier(message) {
    html = '<h5>' + message + '</h5>'
    mandrill('/messages/send', {
        message: {
            to: [{
                email: 'zorgui.oz@gmail.com',
                name: 'Oussama Zorgui'
            }],
            from_email: 'alerts@codeaddicts.io',
            subject: "YunoJunoScraper - Server Status",
            html: html
        }
    }, function (error, response) {
        if (error) console.log(JSON.stringify(error));
        else console.log(response);
    });
}