var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AgencySchema = new Schema({
    url: String,
    id: Number,
    title: String,
    discipline: String,
    start_date: String,
    duration: String,
    location: String,
    description: String,
    creator: String,
    address: String,
    phone: String,
    sent: Boolean
});

module.exports = mongoose.model('Agency', AgencySchema);