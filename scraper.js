var casper = require('casper').create();
var fs = require('fs');
var x = require('casper').selectXPath;
var module = require('functions/module');
var rates = [700, 650, 600, 550, 500, 450, 400, 350, 300, 250, 200, 150, 100, 50]
console.log('working');
casper.start('https://uk.yunojuno.com/sign-in/');

casper.then(function () {
    module.logIn();
});

casper.wait('2000');

casper.wait(500, function () {
    casper.each(Object.keys(rates), function(casper, ind){
        this.then(function() {
            module.changeRate(rates[ind]);
            casper.wait(1000);
            casper.then(function(){
                module.getDashboardProjects(rates[ind]);
            });            
        })
    })
    module.changeRate(350);
})

casper.on('error', function(msg, backtrace) {
    module.notifyServer(JSON.stringify(msg, null, 4) + '<br>' + JSON.stringify(backtrace, null, 4));
    casper.exit(1);
})


casper.wait(2000, function () {
    casper.then(function(){
        this.exit();
    })
})

casper.run();