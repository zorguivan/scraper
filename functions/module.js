module.exports = {
    logIn: function () {
        casper.then(function () {
            this.sendKeys('#id_username', "nathan@codeaddicts.io");
            this.sendKeys('#id_password', "BtkRiB8r6YyyWnVG8QQ");
            console.log('Sending Login Details');
        })
        casper.capture('login.png');

        casper.thenClick(x('/html/body/div[4]/div[2]/div/div/div/div/div[2]/form/button'), function () {

            console.log('Logging-in');
        })
    },

    changeRate: function (rate, callback) {
        casper.thenOpen("https://uk.yunojuno.com/profile/manage/", function () {
            console.log("Redirecting to Profile")
            console.log(rate);
            this.sendKeys("#id_day_rate", rate.toString(), { reset: true });
            this.sendKeys("#id_day_rate", casper.page.event.key.Enter);
        });
        casper.thenClick(x('/html/body/div[4]/div[3]/div/section/div/div[3]/div/form/div[13]/div/div/div/div/button'));

    },
    notifyServer: function (message) {
        console.log('-----')
        console.log(message)
        console.log('-----')
        casper.open("http://127.0.0.1:3003/api/state", {
            method: 'post',
            data: { state: message },
            headers: {
                'Content-type': 'application/x-www-form-urlencoded'
            }
        });
    },

    costomRating: function (id) {
        casper.open("http://127.0.0.1:3003/rated", {
            method: 'post',
            data: { id: id },
            headers: {
                'Content-type': 'application/x-www-form-urlencoded'
            }
        });
    },

    getDashboardProjects: function (iRate) {
        casper.thenOpen("https://uk.yunojuno.com/dashboard/", function () {
            var info = this.evaluate(function () {
                var links = document.querySelectorAll(".ev-fl-Brief--viewBrief");
                // iterate over links and collect stuff
                return Array.prototype.map.call(links, function (link) {
                    return {
                        href: link.href,
                        hrefText: link.textContent.trim()
                    };
                });
            });
            console.log('Searching for Agencies!');
            infoList = info;
        });

        casper.wait(1000);

        casper.then(function () {
            var detailsContainer = [];
            infoList.forEach(function (element) {
                casper.thenOpen(element.href, function (e) {
                    console.log(e)
                    
                    // this.waitForSelector('//*[@id="js-freelancerBriefApply"]/div/button[1]',
                    //     function pass() {
                    //         console.log('found')
                    //     },
                    //     function fail() {
                    //         console.log('Not Found')
                    //     },
                    //     20000 // timeout limit in milliseconds
                    // );


                    var title = this.evaluate(function () {
                        return document.querySelector('.Heading-title').innerHTML || "";
                    });
                    var discipline = this.evaluate(function () {
                        return document.querySelector('.Container-body dl div:nth-child(1) dd').innerHTML || "";
                    });
                    var start_date = this.evaluate(function () {
                        return document.querySelector('.Container-body dl div:nth-child(2) dd').innerHTML || "";
                    });
                    var duration = this.evaluate(function () {
                        console.log('This is duration body');
                        console.log(document.querySelector('.Container-body dl div:nth-child(3) dd').innerHTML)
                        
                        return document.querySelector('.Container-body dl div:nth-child(3) dd').innerHTML || "";
                    });
                    var location = this.evaluate(function () {
                        console.log('This is location body');
                        console.log(document.querySelector('.Container-body dl div:nth-child(4) dd').innerHTML)

                        return document.querySelector('.Container-body dl div:nth-child(4) dd').innerHTML || "";
                    })
                    var description = this.evaluate(function () {
                        return document.querySelector('.Container-body p').innerHTML || "";
                    });
                    var creator = this.evaluate(function () {
                        return document.querySelector('.EmployerInfo h3 a').innerHTML || "";
                    });
                    var address = this.evaluate(function () {
                        return document.querySelectorAll('.EmployerInfo p')[0].innerHTML || "";
                    });
                    var phone = this.evaluate(function () {
                        return document.querySelectorAll('.EmployerInfo p')[1].innerHTML || "";
                    });
                    var rate = iRate;

                    var i_iD = element.href.replace('https://uk.yunojuno.com/work/briefs/', '');
                    var iD = i_iD.replace('/', '');
                    console.log('This is the holder id -> ', iD);
                    var holder = {
                        url: element.href,
                        id: iD.substring(0, iD.length - 1),
                        rate: iRate,
                        title: title,
                        discipline: discipline,
                        start_date: start_date,
                        duration: duration,
                        location: location.split('\n').join('<br>'),
                        description: description.split('\n').join('<br>'),
                        creator: creator.split('\n').join('<br>'),
                        address: address.split('\n').join('<br>'),

                    };

                    if (phone) {
                        holder.phone = phone.split('\n').join('<br>')
                    }

                    detailsContainer.push(holder);
                    console.log(detailsContainer);
                    casper.then(function () {
                        casper.open("http://127.0.0.1:3003/api/agencies/", {
                            method: 'post',
                            data: holder,
                            headers: {
                                'Content-type': 'application/x-www-form-urlencoded'
                            }
                        });
                    });
                    console.log(detailsContainer.length + ' Agencies found of == ' + rate)
                });
            });

        })
    }
};
